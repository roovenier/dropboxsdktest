//
//  ViewController.m
//  DropboxTest
//
//  Created by alexander.oschepkov on 17/08/17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <ObjectiveDropboxOfficial/ObjectiveDropboxOfficial.h>
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([DBClientsManager authorizedClient]) {
        DBUserClient *client = [DBClientsManager authorizedClient];
        
        [[client.filesRoutes listFolder:@""] setResponseBlock:^(DBFILESListFolderResult * _Nullable result, DBFILESListFolderError * _Nullable routeError, DBRequestError * _Nullable networkError) {
            if(result) {
                for (DBFILESMetadata *entry in result.entries) {
                    if ([entry isKindOfClass:[DBFILESFileMetadata class]]) {
                        DBFILESFileMetadata *fileMetadata = (DBFILESFileMetadata *)entry;
                        NSLog(@"File: %@", fileMetadata.name);
                    } else if ([entry isKindOfClass:[DBFILESFolderMetadata class]]) {
                        DBFILESFolderMetadata *folderMetadata = (DBFILESFolderMetadata *)entry;
                        NSLog(@"Folder: %@", folderMetadata.name);
                    }
                }
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if(![DBClientsManager authorizedClient]) {
        [DBClientsManager authorizeFromController:[UIApplication sharedApplication] controller:self openURL:^(NSURL *url) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.dropbox.com/"] options:@{} completionHandler:nil];
        }];
    }
}

#pragma mark - Actions

- (IBAction)dropboxAction:(UIButton *)sender {
    DBUserClient *client = [DBClientsManager authorizedClient];
    
    [[client.filesRoutes listFolder:@""] setResponseBlock:^(DBFILESListFolderResult * _Nullable result, DBFILESListFolderError * _Nullable routeError, DBRequestError * _Nullable networkError) {
        NSLog(@"%@", result);
    }];
}

@end
