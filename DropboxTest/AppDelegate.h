//
//  AppDelegate.h
//  DropboxTest
//
//  Created by alexander.oschepkov on 17/08/17.
//  Copyright © 2017 alexander.oschepkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

